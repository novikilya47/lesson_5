class User {
    constructor(loginStatus){
        this._userId="";
        this._password="";
        this.loginStatus=loginStatus;
        this._registerDate="";
    }

    set userId (id) {
        if(typeof id !=="string"){
            throw new Error("Invalid type of id");
        }
        this._userId = id.trim();
    }

    get userId (){
        return this._userId;
    }

    set password (password) {
        if(typeof password !=="string"){
            throw new Error("Invalid type of password");
        }
        this._password = password.trim();
    }

    get password(){
        return this._password;
    }

    set registerDate (date){
        let day1 = new Date(date);
        let day2 = new Date();
        let newday = (day2.setTime(day1.getTime()));
        if(typeof date !=="string"){
            throw new Error("Invalid type of date");
        } else {
            if (newday<"1625086800000"||newday>"1626296400000"){
                throw new Error("Invalid value of date");
            }
            this._registerDate = date;
        }
    }

    get registerDate(){
        return this._registerDate;
    }


    verifyLogin(){
        const trueId = "1.2.3";
        const truePassword = "qwerty";
        if (this._userId==trueId){
            if(this.password==truePassword){
                console.log(true);
            } else {console.log("Неверный пароль");}
        } else 
        {console.log(false);}
    } 
}

const user = new User ("online");

try {
    user.userId = "1.2.3";
    user.password = "qwerty";
    user.registerDate = "7.2.2021";
} catch (e){
    console.log(e.message);
}

console.log(user);


class ShoppingCart {
    constructor(productID, quantity, dateAdded){
        this._cartID = 0;
        this.productID = parseInt(productID);
        this.quantity = parseInt(quantity);
        this.dateAdded = parseInt(dateAdded);
    }

    set cartID (idcart) {
        idcart = parseInt(idcart);
        if(typeof idcart !=="number"){
            throw new Error("Invalid type of idcart");
        }
        this._cartID = idcart;
    }

    get cartID (){
        return this._cartID;
    }

    addCartItem(){
        console.log("Метод addCartItem вызван");
    }

    updateQuantity(){
        console.log("Метод updateQuantity вызван");
    }

    viewCartDetails(){
        console.log("Метод viewCartDetails вызван");
    }

    checkOut(){
        console.log("Метод checkOut вызван");
    }
}

const shoppingCart = new ShoppingCart ("9", "3", "24");

try {
    shoppingCart.cartID = "19";
} catch (e){
    console.log(e.message);
}

console.log(shoppingCart);


class Customer {
    constructor(email, creditCardInfo, shippingInfo){
        this._customerName ="";
        this._address = "";
        this.email = email;
        this.creditCardInfo = creditCardInfo;
        this.shippingInfo = shippingInfo;
        this._accountBalance = "";
    }

    set customerName (name) {
        if(typeof name !=="string"){
            throw new Error("Invalid type of name");
        }
        this._customerName = name;
    }

    get customerName (){
        return this._customerName;
    }


    set address (adres) {
        if(typeof adres !=="string"){
            throw new Error("Invalid type of address");
        }
        this._address=adres;
    }

    get address (){
        return this._address;
    }

    set accountBalance (balance){
        let floatBalance = parseFloat(balance);
        if(typeof(floatBalance)!=="number"){
            throw new Error("Invalid type of balance");
        }
        this._accountBalance = floatBalance;
    }

    get accountBalance(){
        return this._accountBalance;
    }


    register(){
        console.log("Метод register вызван");
    }

    login(){
        console.log("Метод login вызван");
    }

    updateProfile(){
        console.log("Метод updateProfile вызван");
    }
}

const customer = new Customer ("@mail.ru", "info", "info_2");

try {
    customer.customerName= "dimon";
    customer.address = "улица такая-то";
    customer.accountBalance = "334.87";
} catch (e){
    console.log(e.message);
}

console.log(customer);

class Administrator {
    constructor(email){
        this._adminName ="";
        this.email = email;
    }

    set adminName (name) {
        if(typeof name !=="string"){
            throw new Error("Invalid type of Admin name");
        }
        this._adminName = name;
    }

    get adminName (){
        return this._adminName;
    }

    updateCatalog(){
        console.log(true);
    }
}

const admin = new Administrator("admin@mail.ru");

try{
    admin.adminName = "dimas";
}catch (e){
    console.log(e.message);
}

console.log(admin);


class Order{
    constructor(dateShipped, customerName, customerId, status, shippingId){
        this._orderId = 0;
        this._dateCreated = "";
        this.dateShipped = dateShipped;
        this.customerName = customerName;
        this.customerId = customerId;
        this.status = status;
        this.shippingId = shippingId;
    }

    set orderId (idorder) {
        idorder = parseInt(idorder);
        if(typeof idorder !=="number"){
            throw new Error("Invalid type of order id");
        }
        this._orderId = idorder;
    }

    get orderId (){
        return this._orderId;
    }

    set dateCreated (date) {
        if(typeof date !=="string"){
            throw new Error("Invalid type of date");
        }
        this._dateCreated= date;
    }

    get dateCreated (){
        return this._dateCreated;
    }

    placeOrder(){
        console.log("Метод placeOrder вызван");
    }
}

const order = new Order ("15.09.4567", "Tanya", "1.2.3", "status", "4.5.6");

try{
    order.orderId = "17324234";
    order.dateCreated = "07.07.09";
}catch (e){
    console.log(e.message);
}


console.log(order);


class ShippingInfo{
    constructor(shippingType, shippingCost, shippingRegionId){
        this._shippingId = 0;
        this.shippingType = shippingType;
        this.shippingCost = parseInt(shippingCost);
        this.shippingRegionId = parseInt(shippingRegionId);  
    }

    set shippingId (idshipping) {
        idshipping = parseInt(idshipping);
        if(typeof idshipping !=="number"){
            throw new Error("Invalid type of shipping id");
        }
        this._shippingId = idshipping;
    }

    get shippingId (){
        return this._shippingId;
    }

    updateShippingInfo(){
        console.log("Метод updateShippingInfo вызван");
    }
}

const shippinginfo= new ShippingInfo ("fly", "17.67", "1231513");

try{
    shippinginfo.shippingId = 435435;
}catch (e){
    console.log(e.message);
}

console.log(shippinginfo);


class OrderDetails {
    constructor(productName, quantity, unitCost, subtotal){
        this._orderid =0;
        this._productId =0;
        this.productName = productName;
        this.quantity = quantity;
        this.unitCost = unitCost;
        this.subtotal = subtotal;
    }

    set orderid (idorder) {
        idorder=parseInt(idorder);
        if(typeof idorder !=="number"){
            throw new Error("Invalid type of order id");
        }
        this._orderid= idorder;
    }

    get orderid (){
        return this._orderid;
    }

    set productID(idproduct){
        idproduct=parseInt(idproduct);
        if(typeof idproduct !=="number"){
            throw new Error("Invalid type of product id");
        }
        this._productId= idproduct;
    }

    get productID (){
        return this._productId;
    }

    calcPrice(){
        console.log("Метод calcPrice вызван");
    }
}

const orderdetails = new OrderDetails ("TV", 1, 45.67, 45.67);

try{
    orderdetails.orderid = 12345;
    orderdetails.productID = 54321;
}catch (e){
    console.log(e.message);
}

console.log(orderdetails);










